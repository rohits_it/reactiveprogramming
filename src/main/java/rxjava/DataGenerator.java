package rxjava;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Rohit Sachan on 7/7/2015.
 */
public class DataGenerator {
    public static Iterable<?> generateGreeks() {
        return Arrays.asList("Alpha", "Beta", "Gamma", "Mu", "Rho");
    }

    public static List<String> generateGreeksList() {
        return Arrays.asList("Alpha", "Beta", "Gamma", "Mu", "Rho");
    }
}