package rxjava;

import rx.Observable;
import java.util.List;


/**
 * Created by Rohit on 8/2/2015.
 */
public class FutureCreationExample {

    public static void main(String[] args) {
        Observable<String> observableGreeks = Observable.from(DataGenerator.generateGreeksList()) ;
        observableGreeks.subscribe(
                (i)-> System.out.println(i),
                (e)-> System.out.println("error" + e),
                ()-> System.out.println("copletion")
        );
    }
}
