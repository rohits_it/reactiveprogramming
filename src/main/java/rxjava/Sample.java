package rxjava;

import org.joda.time.DateTime;

import java.util.concurrent.TimeUnit;

/**
 * Created by Rohit Sachan on 7/7/2015.
 */
public class Sample {
    public static void main(String[] args) throws InterruptedException {

        TimeTicker ticker = new TimeTicker(10);
        ticker.start();

        try {
            ticker.toObservable()
                    .sample(1, TimeUnit.SECONDS)
                    .subscribe(t -> System.out.println("tick: " + new DateTime(t).toString()));
            Thread.sleep(10000);
        }finally {

            ticker.stop();
        }
        System.exit(0);
    }

}
